﻿using Pathfinding;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class spawnController : MonoBehaviour
{
    //Array de enemigos disponibles para invocar
    public GameObject[] enemy;
    //Objetivo hacia el que iran los pokemon spawneados
    public GameObject target;
    //Booleanoque bloquea la funcion de spawn
    bool spawn;
    //Booleano para el primer pokemon spawneado
    bool primero;
    //Tiempo entre pokemon invocados
    public float spawnRate;
    //Recurso que aumentara con cada pokemon eliminado
    public Text almas;
    //Vida que disminuira con cada pokemon que llegue al final
    public Text vida;

    // Start is called before the first frame update
    void Start()
    {
        //Les damos a las variables los valores necesarios
        spawn = false;
        primero = true;
        spawnRate = 1f;
        //Invocara la funcion de hacer spawn de enemigos
        Invoke("Spawn", 3f);
        //Invoca la ecorutina que reduce el tiempo entre spawns
        StartCoroutine(ReduceSpawnRate(20f));

    }

    // Update is called once per frame
    void Update()
    {
        //Si esta activado el booleano activara de nuevo la funcion de spawnear enemigos
        if (spawn)
        {
            Spawn();
        }
        //Si la vida del centro pokemon se reduce a 0 se cargara la escena de muerte
        if (vida.text == "0")
        {
           SceneManager.LoadScene("Moriste");
        }


    }

    private void Spawn()
    {
        //Invocara un enemigo aleatorio del array, le pasara el objetivo hacia el que ir y lo posicionara al inicio del recorrido
        GameObject newEnemy = Instantiate(enemy[Random.Range(0, enemy.Length)]);
        newEnemy.GetComponent<AIDestinationSetter>().target = target.transform;
        newEnemy.transform.position = this.transform.position;

        //Le pasara los valores necesarios al enemigo
        newEnemy.GetComponent<snoruntController>().first = primero;
        newEnemy.GetComponent<snoruntController>().almas = almas;
        newEnemy.GetComponent<snoruntController>().vidaCentro = vida;

        primero = false;
        spawn = false;

        //Llamara a la ecorutina para reactivar el spawn de enemigos
        StartCoroutine(Respawn(spawnRate));

    }

    IEnumerator ReduceSpawnRate(float f)
    {
        //Tras f segundos reducira el tiempo entre spawns hasta 0.2 segundos
        yield return new WaitForSeconds(f);
        
        if (spawnRate <= 0.2f)
        {
            spawnRate = 0.2f;
        }
        else
        {
            spawnRate = spawnRate * 0.9f;
            StartCoroutine(ReduceSpawnRate(f));
        }
        
    }

    IEnumerator Respawn(float f)
    {
        //Reactiva el booleano de spawnear enemigos
        yield return new WaitForSeconds(f);
        spawn = true;

    }

}
