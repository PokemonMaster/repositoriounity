﻿using System.Collections;
using UnityEngine;

public class rayoController : MonoBehaviour
{
    //Enemigo al que perseguira el proyectil
    public GameObject enemy;
    //Velocidad de movimiento del proyectil
    public float vel;
    //posicion x del enemigo
    float x;
    //posicion y del enemigo
    float y;

    // Start is called before the first frame update
    void Start()
    {
        //El proyectil sera destruido si no ha impactado tras 1 segundo de ser creado
        StartCoroutine(Muerte(1f));
    }

    // Update is called once per frame
    void Update()
    {
        //igualamos las variables x e y con las de posicion del enemigo
        x = enemy.GetComponent<Transform>().position.x;
        y = enemy.GetComponent<Transform>().position.y;


        if (this.gameObject.transform.position.x < x)
        {

            if (this.gameObject.transform.position.y < y)
            {
                //En el caso de que el proyectil este a la izquierda y abajo del enemigo, este se movera en diagonal a la derecha y hacia arriba
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, vel);
                //Mientras se mueve, el proyectil ira rotando para dar la sensacion de estar animado
                this.gameObject.transform.Rotate(new Vector3(0f, 0f, 135f));

            }
            else if (this.gameObject.transform.position.y > y)
            {
                //En el caso de que el proyectil este a la izquierda y arriba del enemigo, este se movera en diagonal a la derecha y hacia abajo
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, -vel);
                this.gameObject.transform.Rotate(new Vector3(0f, 0f, 45f));
            }
            else
            {
                //En el caso de que el proyectil este a la izquierda del enemigo, este se movera a la derecha
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, 0);
                this.gameObject.transform.Rotate(new Vector3(0f, 0f, 90f));
            }

        }
        else if (this.gameObject.transform.position.x > x)
        {

            if (this.gameObject.transform.position.y < y)
            {
                //En el caso de que el proyectil este a la derecha y abajo del enemigo, este se movera en diagonal a la izquierda y hacia arriba
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, vel);
                this.gameObject.transform.Rotate(new Vector3(0f, 0f, 225f));
            }
            else if (this.gameObject.transform.position.y > y)
            {
                //En el caso de que el proyectil este a la derecha y arriba del enemigo, este se movera en diagonal a la izquierda y hacia abajo
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, -vel);
                this.gameObject.transform.Rotate(new Vector3(0f, 0f, 315f));
            }
            else
            {
                //En el caso de que el proyectil este a la derecha, este se movera a la izquierda
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);
                this.gameObject.transform.Rotate(new Vector3(0f, 0f, 270f));
            }

        }
        else
        {


            if (this.gameObject.transform.position.y < y)
            {
                //En el caso de que el proyectil este por debajo del enemigo, este se movera hacia arriba
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, vel);
                this.gameObject.transform.Rotate(new Vector3(0f, 0f, 180f));
            }
            else if (this.gameObject.transform.position.y > y)
            {
                //En el caso de que el proyectil este por arriba del enemigo, este se movera hacia abajo
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -vel);
                this.gameObject.transform.Rotate(new Vector3(0f, 0f, 0f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            }
        }
        //Si la vida del objetivo cae por debajo de 0 el proyectil se autodestruye
        if (enemy.GetComponent<snoruntController>().vida <= 0)
        {
            Destroy(this.gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si el proyectil colisiona con el enemigo se autodestruye
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }

    }

    IEnumerator Muerte(float f)
    {
        //Tras f tiempo el proyectil se autodestruye
        yield return new WaitForSeconds(f);
        Destroy(this.gameObject);

    }

}














































































































































































//F