﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pikachuController : MonoBehaviour
{
    //GameObject del tipo de proyectil que disparara la torreta
    public GameObject rayo;
    //Booleano que decide si se puede disparar o no
    bool dispara;
    //Cola de pokemon enemigos dentro del rango de ataque
    public Queue<GameObject> cola = new Queue<GameObject>();
    //Pokemon enemigo al que se atacara
    GameObject objective;
    //Tiempo entre disparos
    public float fireRate;
    public bool activo;

    // Start is called before the first frame update
    void Start()
    {
        //Iniciamos las variables como las necesitamos
        dispara = true;
        activo = true;
        //Inicialmente el collider esta en false porque interfiere con el pathing
        this.gameObject.GetComponent<CircleCollider2D>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

        //Revisa si la cola tiene algun enemigo y si el primero de estos tiene menos de 0 de vida lo saca de la cola
        if (cola.Count > 0)
        {
            if (cola.Peek().GetComponent<snoruntController>().vida <= 0)
            {
                cola.Dequeue();
            }

        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si un enemigo entra en el rango de ataque se le metera en la cola de enemigos
        if (collision.gameObject.tag == "Enemy")
        {
            cola.Enqueue(collision.gameObject);
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (activo)
        {
            //Si el collision es un enemigo y el disparo no esta en cooldown se atacara
            if (collision.gameObject.tag == "Enemy")
            {
                if (dispara)
                {
                    if (cola.Count > 0)
                    {
                        //Tras revisarse que haya enemigos en la cola, se creara un proyectil y se le daran los datos necesarios, el objetivo y la posicion inicial
                        GameObject newRayo = Instantiate(rayo);
                        newRayo.GetComponent<rayoController>().enemy = cola.Peek();
                        newRayo.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
                        //Se pone el ataque en cooldown y se inhabilita el ataque temporalmente
                        dispara = false;
                        StartCoroutine(Disparar(fireRate));
                    }

                }

            }
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        //Si el collider que sale del rango de ataque es un enemigo, este se sacara de la cola de objetivos
        if (other.gameObject.tag == "Enemy")
        {
            if (cola.Count > 0)
            {
                cola.Dequeue();
            }
            
        }

    }


    IEnumerator Disparar(float f)
    {
        //Tras esperar f segundos, se reactivara el ataque
        yield return new WaitForSeconds(f);
        dispara = true;

    }


}
