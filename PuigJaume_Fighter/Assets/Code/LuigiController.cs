﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LuigiController : MonoBehaviour
{

    //Este es Luigi Mario, lo mismo que su hermano Mario Mario pero en verde, aunque es mas alto y salta mas todo el mundo presta atencion al rojo ese de mierda
    //Para mas informacion, presta atencion a Luigi Mario Rojo

    public int vel;
    public int jumpForce;
    public GameObject fireBall;
    public Text luigiGana;
    private Animator anim;
    private bool canJump;
    private bool blocking;
    private bool punch1;
    private bool punch2;
    private bool punch3;
    private bool kick1;
    private bool kick2;
    private bool kick3;
    private bool blockPunch;
    private bool blockKick1;
    private bool blockKick2;
    private bool blockKick3;
    private bool fire;
    private bool fireNot;
    private bool direction;
    private bool dead;
    private bool golpeado;
    private bool victoria;
    public float vida;
    public MarioController mario;
    public delegate void OnAtacked(float vida);
    public event OnAtacked OnAtackedEvent;
    public delegate void OnDead();
    public event OnDead OnDeadEvent;
    // Start is called before the first frame update
    void Start()
    {


        anim = GetComponent<Animator>();
        direction = false;
        dead = false;
        golpeado = false;
        mario.OnDeadEvent += Victory;
        luigiGana.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

        anim.SetFloat("Speed", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x));
        anim.SetFloat("VerticalSpeed", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.y));
        anim.SetBool("Block", blocking);
        anim.SetBool("Punch1", punch1);
        anim.SetBool("Punch2", punch2);
        anim.SetBool("Punch3", punch3);
        anim.SetBool("Kick1", kick1);
        anim.SetBool("Kick2", kick2);
        anim.SetBool("Kick3", kick3);
        anim.SetBool("BlockPunch", blockPunch);
        anim.SetBool("BlockKick1", blockKick1);
        anim.SetBool("BlockKick2", blockKick2);
        anim.SetBool("BlockKick3", blockKick3);
        anim.SetBool("Firing", fire);
        anim.SetBool("Golpeado", golpeado);
        anim.SetBool("Dead", dead);
        anim.SetBool("Victory", victoria);

        if (golpeado || dead || victoria)
        {

        }
        else
        {
            if (Input.GetKey("right"))
            {

                if (blocking)
                {

                }
                else
                {

                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);

                }
                direction = true;
                transform.localScale = new Vector3(0.9f, 1.2f, 1f);

            }
            else if (Input.GetKey("left"))
            {

                if (blocking)
                {

                }
                else
                {

                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);

                }
                direction = false;
                transform.localScale = new Vector3(-0.9f, 1.2f, 1f);

            }
            else
            {

                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);

            }

            if (Input.GetKey("up") && canJump)
            {

                this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

                canJump = false;

                blocking = false;

                anim.SetBool("Jumping", true);

            }

            if (Input.GetKey("down"))
            {
                if (anim.GetBool("Jumping"))
                {
                    blocking = false;
                }
                else
                {
                    blocking = true;
                }

            }
            else
            {
                blocking = false;
            }

            if (Input.GetKeyDown("[8]"))
            {

                if (blocking)
                {

                    if (!blockKick1 && !blockKick2 && !blockKick3)
                    {
                        blockPunch = false;
                        blockKick1 = true;
                        StartCoroutine(BlockHit(.5f, 2));
                        Debug.Log("block kick1");
                    }
                    else if (blockKick1 && !blockKick2 && !blockKick3)
                    {
                        blockKick1 = false;
                        blockKick2 = true;
                        StartCoroutine(BlockHit(.5f, 3));
                        Debug.Log("block kick2");
                    }
                    else if (!blockKick1 && blockKick2 && !blockKick3)
                    {
                        blockKick2 = false;
                        blockKick3 = true;
                        StartCoroutine(BlockHit(.5f, 4));
                        Debug.Log("block kick3");
                    }
                    else if (!blockKick1 && !blockKick2 && blockKick3)
                    {
                        blockKick1 = false;
                        blockKick2 = false;
                        blockKick3 = false;
                    }
                }
                else
                {

                    if (!kick1 && !kick2 && !kick3)
                    {

                        kick1 = true;
                        anim.SetBool("Punch1", punch1);
                        StartCoroutine(Kick(.5f, 1));
                        Debug.Log("petada1");

                    }
                    else if (kick1 && !kick2 && !kick3)
                    {

                        kick1 = false;
                        kick2 = true;
                        StartCoroutine(Kick(.5f, 2));
                        Debug.Log("petada2");
                    }
                    else if (!kick1 && kick2 && !kick3)
                    {

                        kick2 = false;
                        kick3 = true;
                        StartCoroutine(Kick(.5f, 3));
                        Debug.Log("petada3");
                    }
                    else if (!kick1 && !kick2 && kick3)
                    {
                        kick1 = false;
                        kick2 = false;
                        kick3 = false;
                        Debug.Log("petada fin");
                    }


                }

            }


            if (Input.GetKeyDown("[9]"))
            {

                if (blocking)
                {

                    if (!blockPunch)
                    {
                        blockKick1 = false;
                        blockKick2 = false;
                        blockKick3 = false;
                        blockPunch = true;
                        StartCoroutine(BlockHit(.3f, 1));
                        Debug.Log("block punch");
                    }

                }
                else
                {
                    if (!punch1 && !punch2 && !punch3)
                    {

                        punch1 = true;
                        anim.SetBool("Punch1", punch1);
                        StartCoroutine(Punch(.5f, 1));
                        Debug.Log("puñetaso1");

                    }
                    else if (punch1 && !punch2 && !punch3)
                    {

                        punch1 = false;
                        punch2 = true;
                        StartCoroutine(Punch(.5f, 2));
                        Debug.Log("puñetaso2");
                    }
                    else if (!punch1 && punch2 && !punch3)
                    {

                        punch2 = false;
                        punch3 = true;
                        StartCoroutine(Punch(.5f, 3));
                        Debug.Log("puñetaso3");
                    }
                }



            }

            if (Input.GetKeyDown("[7]"))
            {
                if (!punch1 && !punch2 && !punch3 && !blockPunch && !kick1 && !kick2 && !kick3 && !blockKick1 && !blockKick2 && !blockKick3 && !blocking && !fireNot)
                {
                    fire = true;
                    StartCoroutine(EndFire(.5f));
                    GameObject newFire = Instantiate(fireBall);

                    if (!direction)
                    {
                        newFire.GetComponent<LuigiFire>().vel = newFire.GetComponent<LuigiFire>().vel*-1;
                        newFire.transform.position = new Vector2(this.transform.position.x - 1, this.transform.position.y + 1.1f);
                    }
                    else
                    {
                        
                        newFire.transform.position = new Vector2(this.transform.position.x + 1, this.transform.position.y + 1.1f);
                    }

                    fireNot = true;
                    StartCoroutine(DontFire(.5f));
                }


            }
        }



        if (vida==0)
        {
            OnDeadEvent();
            Die();
        }

    }

    private void Victory()
    {

        victoria = true;
        luigiGana.gameObject.SetActive(true);
    }

    private void Die()
    {

        OnDeadEvent();
        dead = true;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Floor")
        {
            canJump = true;
            anim.SetBool("Jumping", false);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (!blocking)
        {
            if (collision.gameObject.name == "Ataque")
            {
                if (blocking)
                {
                    vida -= 2;
                }
                else
                {
                    vida -= 4;
                }

                if (direction)
                {
                    this.transform.position = new Vector2(this.transform.position.x - 0.1f, this.transform.position.y);
                }
                else
                {
                    this.transform.position = new Vector2(this.transform.position.x + 0.1f, this.transform.position.y);
                }

                if (vida < 0)
                {
                    vida = 0;
                }

                OnAtackedEvent(vida);

                if (vida <= 0)
                {
                    dead = true;
                }
                golpeado = true;
                StartCoroutine(Pegame(.2f));
            }
            else if (collision.gameObject.name == "Patada")
            {

                if (blocking)
                {
                    vida -= 3;
                }
                else
                {
                    vida -= 6;
                }

                if (direction)
                {
                    this.transform.position = new Vector2(this.transform.position.x - 0.1f, this.transform.position.y);
                }
                else
                {
                    this.transform.position = new Vector2(this.transform.position.x + 0.1f, this.transform.position.y);
                }

                if (vida < 0)
                {
                    vida = 0;
                }

                OnAtackedEvent(vida);

                if (vida <= 0)
                {
                    dead = true;
                }
                golpeado = true;
                StartCoroutine(Pegame(.2f));
            }
            else if (collision.gameObject.name == "MarioBall")
            {

                if (blocking)
                {
                    vida -= 1;
                }
                else
                {
                    vida -= 2;
                }

                if (vida < 0)
                {
                    vida = 0;
                }

                OnAtackedEvent(vida);

                if (vida <= 0)
                {
                    dead = true;
                }

                golpeado = true;
                StartCoroutine(Pegame(.2f));

            }
            else if (collision.gameObject.name == "Lava")
            {

                vida = 0;
                OnAtackedEvent(vida);

                if (vida <= 0)
                {
                    dead = true;
                }

            }



        }
        else
        {

        }


    }


    IEnumerator Pegame(float f)
    {

        yield return new WaitForSeconds(f);

        golpeado = false;

    }

    IEnumerator BlockHit(float f, int n)
    {

        yield return new WaitForSeconds(f);
        if (n == 1)
        {
            blockPunch = false;
        }
        else if (n == 2)
        {
            blockKick1 = false;
        }
        else if (n == 3)
        {
            blockKick2 = false;
        }
        else if (n == 4)
        {
            blockKick3 = false;
        }

    }

    IEnumerator Punch(float f, int n)
    {

        yield return new WaitForSeconds(f);
        if (n == 1)
        {
            punch1 = false;
        }
        else if (n == 2)
        {
            punch2 = false;
        }
        else if (n == 3)
        {
            punch3 = false;
        }

    }

    IEnumerator Kick(float f, int n)
    {

        yield return new WaitForSeconds(f);
        if (n == 1)
        {
            kick1 = false;
        }
        else if (n == 2)
        {
            kick2 = false;
        }
        else if (n == 3)
        {
            kick3 = false;
        }

    }

    IEnumerator EndFire(float f)
    {

        yield return new WaitForSeconds(f);
        fire = false;

    }

    IEnumerator DontFire(float f)
    {

        yield return new WaitForSeconds(f);
        fireNot = false;

    }
    
}
