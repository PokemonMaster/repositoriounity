﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hachaController : MonoBehaviour
{

    private int vida;

    public delegate void OnAtacked();
    public event OnAtacked OnAtackedEvent;
    // Start is called before the first frame update
    void Start()
    {
        vida = 5;
    }

    // Update is called once per frame
    void Update()
    {
        //Si la vida del hacha llega a 0 el puente del castillo caera.
        //La vida del hachase reiniciara al cabo de 10 segundos.
        if (vida <= 0)
        {
            OnAtackedEvent();
            StartCoroutine(Regenerate(10f));
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.name == "AtaqueL" || collision.gameObject.name == "PatadaL" || collision.gameObject.name == "LuigiBall" || collision.gameObject.name == "Ataque" || collision.gameObject.name == "Patada" || collision.gameObject.name == "MarioBall")
        {
            vida--;
        }

    }

    IEnumerator Regenerate(float f)
    {

        yield return new WaitForSeconds(f);
        vida = 5;

    }

}
