﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuigiFire : MonoBehaviour
{
    public int vel;
    public int velVertical;
    private bool change;

    // Start is called before the first frame update
    void Start()
    {
        
        change = true;
        //Al cabo de 5 segundos de crearse el objeto este sera destruido
        StartCoroutine(Die(5f));
    }

    // Update is called once per frame
    void Update()
    {

        //movimiento en zig-zag
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, velVertical);
        if (change)
        {

            StartCoroutine(Change(0.2f));
            change = false;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Floor" || collision.gameObject.name == "fireBallLuigi" || collision.gameObject.tag == "Player2")
        {

        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    //Cambia la direccion vertical de la bola de fuego
    IEnumerator Change(float f)
    {

        yield return new WaitForSeconds(f);
        velVertical = velVertical * (-1);
        change = true;
        
    }

    IEnumerator Die(float f)
    {

        yield return new WaitForSeconds(f);
        Destroy(this.gameObject);

    }
}
