﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerController : MonoBehaviour
{
    public GameObject cosa;
    public MenuInventario inventMenu;
    public int vel;
    //instancia del singleton
    static GameObject player = null;
    private Animator anim;
    public bool subo = false;
    public Creature[] party;
    public Camera gameCamera;
    bool pueblo;
    bool overworld;
    float comb;
    float aCombatir;
    bool inv;
    public bool combatiendo;
    int laugh;
    int ahora;
    AudioSource audioData;
    List<Item> inventario = new List<Item>();
    Creature[] alCargar = new Creature[4];

    void Awake()
    {
        //Patró singleton
        if (player == null)
        {
            //crea el objecte en el primer moment
            player = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena. 
            //D'aquesta manera no es destrueix al carretgarse
            DontDestroyOnLoad(player);
        }
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }
        gameCamera = Camera.main;

    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        audioData = GetComponent<AudioSource>();

        pueblo = true;

        comb = 0;
        aCombatir = Random.Range(200, 300);
        laugh = Random.Range(0, 500);

        inventario.Add(new Item("Pocion", "8 andaluz", "Cura un poco supongo"));
        inventario.Add(new Item("Ataque X", "Infinito", "Dobla el ataque"));
        inventario.Add(new Item("Poke ball", "1", "Desconoces su uso"));

        for(int i = 0; i<3; i++)
        {
            inventMenu.items[i].text = inventario[i].nom;
            inventMenu.cants[i].text = "x"+inventario[i].quant;
        }
        inventMenu.gameObject.SetActive(false);
        for (int i = 0; i < 3; i++)
        {
            inventMenu.items[i].gameObject.SetActive(false);
            inventMenu.cants[i].gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

        anim.SetFloat("Speed", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x));
        anim.SetFloat("Vert", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.y));
        if (combatiendo && !overworld)
        {

            
            this.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);

        }
        else if (inv)
        {

            if (Input.GetKey("f"))
            {

                inv = false;
                inventMenu.gameObject.SetActive(false);

                for (int i = 0; i < 3; i++)
                {
                    inventMenu.items[i].gameObject.SetActive(false);
                    inventMenu.cants[i].gameObject.SetActive(false);

                }

            }

        }
        else
        {

            this.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

            if (Input.GetKey("p"))
            {

                inv = true;
                inventMenu.gameObject.SetActive(true);
                transform.localScale = new Vector3(1f, 1f, 1f);
                inventMenu.gameObject.transform.localScale = new Vector3(52.09999f, 52.09999f, 52.09999f);

                for (int i = 0; i < 3; i++)
                {
                    inventMenu.items[i].gameObject.SetActive(true);
                    inventMenu.cants[i].gameObject.SetActive(true);
                    inventMenu.items[i].gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                    inventMenu.cants[i].gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                    
                }

            }

            if (Input.GetKey("a"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                inventMenu.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                inventMenu.items[0].gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                transform.localScale = new Vector3(1f, 1f, 1f);


            }
            else if (Input.GetKey("d"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                inventMenu.gameObject.transform.localScale = new Vector3(-1f, 1f, 1f);
                transform.localScale = new Vector3(-1f, 1f, 1f);
                inventMenu.items[0].gameObject.transform.localScale = new Vector3(-1f, 1f, 1f);

            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);

            }
            if (Input.GetKey("w"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
                subo = true;
            }
            else if (Input.GetKey("s"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                subo = false;
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);

            }

            if (comb == aCombatir)
            {
                comb = 0;
                overworld = false;
                Invoke("Combate", 0.0f);


            }
            else if (overworld)
            {
                comb++;
            }

            if (pueblo)
            {

                if (Input.GetKey("g"))
                {
                    this.saveGame();
                }
                if (Input.GetKey("h"))
                {
                    this.LoadGame();
                }

            }

        }

        if (ahora == laugh)
        {
            audioData.Play(0);
            laugh = Random.Range(100, 1000);
            ahora = 0;
        }
        else
        {
            
            ahora++;
        }

        Debug.Log(aCombatir);
        Debug.Log(comb);
    }

    private Save createSave()
    {
        Save save = new Save();

        CopiaCreature[] falsaparty = new CopiaCreature[4];

        for(int i = 0; i < 4; i++)
        {

            CopiaCreature cpy = new CopiaCreature();

            cpy.atk = this.party[i].atk;
            cpy.hp = this.party[i].hp;
            cpy.maxhp = this.party[i].maxhp;
            cpy.mp = this.party[i].mp;
            cpy.maxmp = this.party[i].maxmp;
            cpy.spd = this.party[i].spd;
            cpy.mAtk = this.party[i].mAtk;
            cpy.lvl = this.party[i].lvl;
            cpy.defensa = this.party[i].defensa;
            cpy.player = this.party[i].player;
            cpy.ranger = this.party[i].ranger;
            cpy.spells = this.party[i].spells;
            cpy.dead = this.party[i].dead;
            cpy.exp = this.party[i].exp;

            CopiaAbilities cpyAbi = new CopiaAbilities();

            cpyAbi.name = this.party[i].habilidades.name;
            cpyAbi.coste = this.party[i].habilidades.coste;
            cpyAbi.dmg = this.party[i].habilidades.dmg;

            cpy.habilidades = cpyAbi;

            falsaparty[i] = cpy;

        }

        save.party = falsaparty;
        save.x = this.transform.position.x;
        save.y = this.transform.position.y;

        return save;

    }

    private void saveGame()
    {
        //crees el objecte save
        Save save = this.createSave();
        //crees un BinaryFormatter que es com un OOS
        BinaryFormatter bf = new BinaryFormatter();
        //Crees el File
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        //serialitzes
        bf.Serialize(file, save);
        file.Close();

        Debug.Log("Game Saved");

    }

    public void LoadGame()
    {
        // 1
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {


            // 2
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            file.Close();

            // 3
            this.transform.position = new Vector2(save.x, save.y);

            for (int i = 0; i < 4; i++)
            {
                /*
                Creature pers = (Creature)Instantiate(this.party[i], transform.position, transform.rotation);
                
                pers.atk = save.party[i].atk;
                pers.hp = save.party[i].hp;
                pers.maxhp = save.party[i].maxhp;
                pers.mp = save.party[i].mp;
                pers.maxmp = save.party[i].maxmp;
                pers.mAtk = save.party[i].mAtk;
                pers.lvl = save.party[i].lvl;
                pers.defensa = save.party[i].defensa;
                pers.player = save.party[i].player;
                pers.ranger = save.party[i].ranger;
                pers.spells = save.party[i].spells;
                pers.dead = save.party[i].dead;
                pers.exp = save.party[i].exp;
                */
                Abilities abi = (Abilities)Instantiate(this.party[i].habilidades, transform.position, transform.rotation);

                abi.name = save.party[i].habilidades.name;
                abi.coste = save.party[i].habilidades.coste;
                abi.dmg = save.party[i].habilidades.dmg;
                //pers.habilidades = abi;

                if(save.party[i].ranger == 1)
                {

                    GameObject.Find("Hero1").GetComponent<Creature>().atk = save.party[i].atk;
                    GameObject.Find("Hero1").GetComponent<Creature>().hp = save.party[i].hp;
                    GameObject.Find("Hero1").GetComponent<Creature>().maxhp = save.party[i].maxhp;
                    GameObject.Find("Hero1").GetComponent<Creature>().mp = save.party[i].mp;
                    GameObject.Find("Hero1").GetComponent<Creature>().maxmp = save.party[i].maxmp;
                    GameObject.Find("Hero1").GetComponent<Creature>().mAtk = save.party[i].mAtk;
                    GameObject.Find("Hero1").GetComponent<Creature>().defensa = save.party[i].defensa;
                    GameObject.Find("Hero1").GetComponent<Creature>().player = save.party[i].player;
                    GameObject.Find("Hero1").GetComponent<Creature>().ranger = save.party[i].ranger;
                    GameObject.Find("Hero1").GetComponent<Creature>().spells = save.party[i].spells;
                    GameObject.Find("Hero1").GetComponent<Creature>().dead = save.party[i].dead;
                    GameObject.Find("Hero1").GetComponent<Creature>().exp = save.party[i].exp;
                    GameObject.Find("Hero1").GetComponent<Creature>().habilidades = abi;

                }
                else if (save.party[i].ranger == 2)
                {
                    GameObject.Find("Hero2").GetComponent<Creature>().atk = save.party[i].atk;
                    GameObject.Find("Hero2").GetComponent<Creature>().hp = save.party[i].hp;
                    GameObject.Find("Hero2").GetComponent<Creature>().maxhp = save.party[i].maxhp;
                    GameObject.Find("Hero2").GetComponent<Creature>().mp = save.party[i].mp;
                    GameObject.Find("Hero2").GetComponent<Creature>().maxmp = save.party[i].maxmp;
                    GameObject.Find("Hero2").GetComponent<Creature>().mAtk = save.party[i].mAtk;
                    GameObject.Find("Hero2").GetComponent<Creature>().defensa = save.party[i].defensa;
                    GameObject.Find("Hero2").GetComponent<Creature>().player = save.party[i].player;
                    GameObject.Find("Hero2").GetComponent<Creature>().ranger = save.party[i].ranger;
                    GameObject.Find("Hero2").GetComponent<Creature>().spells = save.party[i].spells;
                    GameObject.Find("Hero2").GetComponent<Creature>().dead = save.party[i].dead;
                    GameObject.Find("Hero2").GetComponent<Creature>().exp = save.party[i].exp;
                    GameObject.Find("Hero2").GetComponent<Creature>().habilidades = abi;
                }
                else if (save.party[i].ranger == 3)
                {
                    GameObject.Find("Hero3").GetComponent<Creature>().atk = save.party[i].atk;
                    GameObject.Find("Hero3").GetComponent<Creature>().hp = save.party[i].hp;
                    GameObject.Find("Hero3").GetComponent<Creature>().maxhp = save.party[i].maxhp;
                    GameObject.Find("Hero3").GetComponent<Creature>().mp = save.party[i].mp;
                    GameObject.Find("Hero3").GetComponent<Creature>().maxmp = save.party[i].maxmp;
                    GameObject.Find("Hero3").GetComponent<Creature>().mAtk = save.party[i].mAtk;
                    GameObject.Find("Hero3").GetComponent<Creature>().defensa = save.party[i].defensa;
                    GameObject.Find("Hero3").GetComponent<Creature>().player = save.party[i].player;
                    GameObject.Find("Hero3").GetComponent<Creature>().ranger = save.party[i].ranger;
                    GameObject.Find("Hero3").GetComponent<Creature>().spells = save.party[i].spells;
                    GameObject.Find("Hero3").GetComponent<Creature>().dead = save.party[i].dead;
                    GameObject.Find("Hero3").GetComponent<Creature>().exp = save.party[i].exp;
                    GameObject.Find("Hero3").GetComponent<Creature>().habilidades = abi;
                }
                else if (save.party[i].ranger == 4)
                {
                    GameObject.Find("Hero4").GetComponent<Creature>().atk = save.party[i].atk;
                    GameObject.Find("Hero4").GetComponent<Creature>().hp = save.party[i].hp;
                    GameObject.Find("Hero4").GetComponent<Creature>().maxhp = save.party[i].maxhp;
                    GameObject.Find("Hero4").GetComponent<Creature>().mp = save.party[i].mp;
                    GameObject.Find("Hero4").GetComponent<Creature>().maxmp = save.party[i].maxmp;
                    GameObject.Find("Hero4").GetComponent<Creature>().mAtk = save.party[i].mAtk;
                    GameObject.Find("Hero4").GetComponent<Creature>().defensa = save.party[i].defensa;
                    GameObject.Find("Hero4").GetComponent<Creature>().player = save.party[i].player;
                    GameObject.Find("Hero4").GetComponent<Creature>().ranger = save.party[i].ranger;
                    GameObject.Find("Hero4").GetComponent<Creature>().spells = save.party[i].spells;
                    GameObject.Find("Hero4").GetComponent<Creature>().dead = save.party[i].dead;
                    GameObject.Find("Hero4").GetComponent<Creature>().exp = save.party[i].exp;
                    GameObject.Find("Hero4").GetComponent<Creature>().habilidades = abi;
                }



                //pers.gameObject.GetComponent<Transform>().SetParent(this.party[i]);

                //this.party[i] = pers.gameObject.AddComponent<Creature>();
                //this.alCargar[i] = pers;
                //this.party[i] = this.alCargar[i].gameObject.AddComponent<Creature>();

            }


            Debug.Log("Game Loaded");

        }
        else
        {
            Debug.Log("No game saved!");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name == "Moverse")
        {
            
            this.transform.position = new Vector2(-4.5f, -3f);
            pueblo = false;
            overworld = true;
            //Invoke("Combate", Random.Range(4.0f, 10.0f));

            SceneManager.LoadScene("Overworld");
        }

        //Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name == "aPueblo")
        {

            this.transform.position = new Vector2(-3.5f, -11.3f);
            pueblo = true;
            overworld = false;
            comb = 0;

            SceneManager.LoadScene("Pruebas");
        }

        //Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name == "aCastillo")
        {

            this.transform.position = new Vector2(-3.5f, -10.5f);
            overworld = false;
            comb = 0;
            SceneManager.LoadScene("castillo");
        }

        if (collision.gameObject.name == "aBoss")
        {
            SceneManager.LoadScene("combateCastillo");
        }

        if (collision.gameObject.name == "aOver")
        {
            
            this.transform.position = new Vector2(5.5f, 1.5f);
            overworld = true;

            SceneManager.LoadScene("Overworld");
        }

        
    }

    void Combate()
    {
        combatiendo = true;
        cosa.SetActive(false);
        SceneManager.LoadScene("Combat");
        cosa.SetActive(true);
    }

}
