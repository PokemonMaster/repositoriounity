﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{

    public string nom;
    public string quant;
    public string desc;

    public Item(string n, string q, string d)
    {
        nom = n;
        quant = q;
        desc = d;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
