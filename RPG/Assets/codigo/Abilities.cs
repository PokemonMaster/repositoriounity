﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Abilities : MonoBehaviour
{

    public string name;
    public int coste;
    public int dmg;

    public Abilities(string nom, int cost, int damage)
    {
        name = nom;
        coste = cost;
        dmg = damage;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
