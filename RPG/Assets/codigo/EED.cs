﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EED : MonoBehaviour

    //Activar musica del EASTER EGG
{

    public AudioSource music;
    // Start is called before the first frame update
    void Start()
    {
        music = GetComponent<AudioSource>();
    }



    // Update is called once per frame
    void Update()
    {

    }

    //Colision del easter egg contra el personaje
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "rojo_6")
        {
            music.Play();

        }

    }
}
