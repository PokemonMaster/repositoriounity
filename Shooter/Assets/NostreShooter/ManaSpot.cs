﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaSpot : MonoBehaviour
{

    public GameObject manaSkull;
    public GameObject particles;
    public bool active;

    // Start is called before the first frame update
    void Start()
    {
        manaSkull.SetActive(false);
        particles.SetActive(false);
        Invoke("Invocacio", 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Invocacio()
    {

        manaSkull.SetActive(true);
        particles.SetActive(true);
        active = true;

    }

    public void pickUp()
    {
        Debug.Log("BBBBBBBBBBBBB");
        manaSkull.SetActive(false);
        particles.SetActive(false);
        active = false;
        float timeGap = Random.Range(5.0f, 15.0f);
        Invoke("Invocacio", timeGap);

    }

}
