﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pincha : MonoBehaviour
{


    public GameEvent pegar;
    public GameObject objetivo;
    public GameObject manaspot1;
    public GameObject manaspot2;
    public bool mana1;
    public bool mana2;
    public bool fear;
    public bool wind;
    public bool burningMan;
    public bool ignore;
    public bool ignoring;
    public GameObject particles;
    public GameObject viento;
    public GameObject fire;

    // Start is called before the first frame update
    void Start()
    {
        fear = false;
        ignoring = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (fear)
        {

            Vector3 playerDirec = transform.position - objetivo.transform.position;
            Vector3 newDir = transform.position + playerDirec;
            this.GetComponent<NavMeshAgent>().SetDestination(newDir);
            this.GetComponent<NavMeshAgent>().speed = 3;

        }
        else if (wind)
        {

            Vector3 playerDirec = transform.position - objetivo.transform.position;
            Vector3 newDir = transform.position + playerDirec;
            this.GetComponent<NavMeshAgent>().SetDestination(newDir);
            this.GetComponent<NavMeshAgent>().speed = 8;
            this.transform.Rotate(new Vector3(0f,30f,0f), Space.Self);

        }
        else
        {
            this.GetComponent<NavMeshAgent>().speed = 5;
            if (!ignoring)
            {

                int ignoreTime = Random.Range(1, 100);
                if(ignoreTime == 1)
                {
                    ignore = true;
                    Invoke("StopIgnoring", 3f);
                }

            }
            if (ignore)
            {

                int randomDir = Random.Range(0, 4);
                Vector3 direction;
                if (randomDir == 0)
                {
                    direction = new Vector3(-13f, 2.35f, -12f);
                }
                else if (randomDir == 1)
                {
                    direction = new Vector3(28f, 2.35f, -12f);
                }
                else if (randomDir == 2)
                {
                    direction = new Vector3(-13f, 2.35f, 30f);
                }
                else
                {
                    direction = new Vector3(28f, 2.35f, 30f);
                }
                

                this.GetComponent<NavMeshAgent>().SetDestination(direction);

            }
            else
            {
                Vector3 playerDirec = transform.position - objetivo.transform.position;
                if (manaspot1.GetComponent<ManaSpot>().active)
                {
                    mana1 = true;
                }
                else
                {
                    mana1 = false;
                }
                if (manaspot2.GetComponent<ManaSpot>().active)
                {
                    mana2 = true;
                }
                else
                {
                    mana2 = false;
                }
                Vector3 spotOneDirec = transform.position - manaspot1.transform.position;
                Vector3 spotTwoDirec = transform.position - manaspot2.transform.position;

                if (playerDirec.magnitude < spotOneDirec.magnitude)
                {

                    if (playerDirec.magnitude < spotTwoDirec.magnitude)
                    {

                        this.GetComponent<NavMeshAgent>().SetDestination(objetivo.transform.position);

                    }
                    else
                    {

                        if (mana2)
                        {
                            this.GetComponent<NavMeshAgent>().SetDestination(manaspot2.transform.position);
                        }
                        else
                        {
                            this.GetComponent<NavMeshAgent>().SetDestination(objetivo.transform.position);
                        }


                    }

                }
                else
                {

                    if (spotOneDirec.magnitude < spotTwoDirec.magnitude)
                    {

                        if (mana1)
                        {
                            this.GetComponent<NavMeshAgent>().SetDestination(manaspot1.transform.position);
                        }
                        else
                        {
                            this.GetComponent<NavMeshAgent>().SetDestination(objetivo.transform.position);
                        }

                    }
                    else
                    {

                        if (mana2)
                        {
                            this.GetComponent<NavMeshAgent>().SetDestination(manaspot2.transform.position);
                        }
                        else
                        {
                            this.GetComponent<NavMeshAgent>().SetDestination(objetivo.transform.position);
                        }

                    }

                }
            }
            

            
        }
        
        

    }

    public void StopIgnoring()
    {
        ignore = false;
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.transform.tag == "Player")
        {

            pegar.Raise();

        }

    }

    public void burns()
    {

        burningMan = true;
        fire.SetActive(true);
        StartCoroutine(burnin(3));

    }

    private IEnumerator burnin(float waitTime)
    {

        yield return new WaitForSeconds(waitTime/3);
        this.gameObject.GetComponent<Disparable>().dano(5);
        yield return new WaitForSeconds((waitTime*2) / 3);
        this.gameObject.GetComponent<Disparable>().dano(5);
        yield return new WaitForSeconds((waitTime * 3) / 3);
        this.gameObject.GetComponent<Disparable>().dano(3);
        burningMan = false;
        fire.SetActive(false);

    }

    public void windy()
    {

        wind = true;
        viento.SetActive(true);
        StartCoroutine(blowed(2));

    }

    private IEnumerator blowed(float waitTime)
    {

        yield return new WaitForSeconds(waitTime);
        wind = false;
        viento.SetActive(false);

    }

    public void scary()
    {

        fear = true;
        particles.SetActive(true);
        StartCoroutine(scared(5));

    }

    private IEnumerator scared(float waitTime)
    {

        yield return new WaitForSeconds(waitTime);
        fear = false;
        particles.SetActive(false);

    }

}
