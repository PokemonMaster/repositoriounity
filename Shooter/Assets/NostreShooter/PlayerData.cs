﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New PlayerData", menuName = "Player Data", order = 52)]
public class PlayerData : ScriptableObject
{

    [SerializeField]
    public float hp;
    [SerializeField]
    public float mana;
    [SerializeField]
    public int nBombs;

}
