﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparable : MonoBehaviour
{
    public float hp = 50f;
    public GameObject ragdoll;
    public GameObject vida;
    //public PlayerData player;

    public void dano(float damage)
    {

        this.GetComponent<AudioSource>().Play();

        //player.hp -= damage;
        hp -= damage;

        if (this.name == "ZombieRig")
        {


        }
        else
        {

            vida.transform.localScale = new Vector3(hp / 50, 0.1f, 0.2f);

        }

        

        if (hp <= 0)
        {
            /*
            if (this.name == "ZombieRig")
            {
                GameObject zombieRagdoll = Instantiate(ragdoll,this.transform.position,this.transform.rotation);

                //Destroy(this.gameObject);

            }
            if (this.name == "DogPBR")
            {
                GameObject dogRagdoll = Instantiate(ragdoll, this.transform.position, this.transform.rotation);

                //Destroy(this.gameObject);

            }
            */

            GameObject dogRagdoll = Instantiate(ragdoll, this.transform.position, this.transform.rotation);

            Destroy(this.gameObject);
        }
    }

}
