﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escopeta : MonoBehaviour
{
    float damage = 10f;
    float range = 20000f;
    float momentum = 200f;
    public Camera FPCamera;
    public GameObject bengala;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
        if (Input.GetKeyDown("1"))
        {
            this.gameObject.GetComponent<Escopeta>().enabled = false;
            this.gameObject.GetComponent<Arma>().enabled = true;
        }
        if (Input.GetKeyDown("2"))
        {
            this.gameObject.GetComponent<AK>().enabled = true;
            this.gameObject.GetComponent<Escopeta>().enabled = false;
        }
    }

    private void Shoot()
    {

        for(int i = 0; i < 10;  i++)
        {
            Vector3 newForward = new Vector3(FPCamera.transform.forward.x + UnityEngine.Random.Range(-365f, 365f), FPCamera.transform.up.y + UnityEngine.Random.Range(-365f, 365f), FPCamera.transform.right.z + UnityEngine.Random.Range(-365f, 365f));
            RaycastHit hit;
            if (Physics.Raycast(FPCamera.transform.position, newForward, out hit, range))
            {
                Debug.DrawRay(FPCamera.transform.position, newForward, Color.red, 1f);
                print(hit.transform.name);
                if (hit.transform.tag == "Disparable")
                {
                    hit.transform.gameObject.GetComponent<Disparable>().dano(damage);
                    //Vector3 middle = new Vector3(hit.normal.x, )
                    hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
                }
                GameObject newBengala = Instantiate(bengala);
                newBengala.transform.position = hit.point;
                Destroy(newBengala, 1f);

            }
        }
        

    }

}
