﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    float damage = 10f;
    float range = 50f;
    float momentum = 100f;
    public Camera FPCamera;
    public GameObject bengala;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
        if (Input.GetKeyDown("3"))
        {
            this.gameObject.GetComponent<Escopeta>().enabled = true;
            this.gameObject.GetComponent<Arma>().enabled = false;
        }
        if (Input.GetKeyDown("2"))
        {
            this.gameObject.GetComponent<AK>().enabled = true;
            this.gameObject.GetComponent<Arma>().enabled = false;
        }
    }

    private void Shoot()
    {
        RaycastHit hit;
        

        if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
        {
            Debug.DrawLine(FPCamera.transform.position, hit.point, Color.red,1f);
            print(hit.transform.name);
            if (hit.transform.tag == "Disparable")
            {
                hit.transform.gameObject.GetComponent<Disparable>().dano(damage);
                //Vector3 middle = new Vector3(hit.normal.x, )
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * momentum, hit.point);
            }

            GameObject newBengala = Instantiate(bengala);
            newBengala.transform.position = hit.point;
            Destroy(newBengala, 1f);


        }

    }
    

}
