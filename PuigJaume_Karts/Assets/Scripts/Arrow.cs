﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{

    public bool dir;

    // Start is called before the first frame update
    void Start()
    {
        dir = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (dir)
        {
            this.GetComponent<Rigidbody>().velocity = this.transform.up * -5f;
        }
        else
        {
            this.GetComponent<Rigidbody>().velocity = this.transform.up * 5f;
        }

    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag.Equals("limite"))
        {

            dir = !dir;

        }

    }

}
