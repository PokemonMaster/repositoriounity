﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurboController : MonoBehaviour
{

    public JugadorController jug;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider collider)
    {

        if (collider.gameObject.name.Equals("Player"))
        {

            StartCoroutine(Turbo());

        }

    }

    public IEnumerator Turbo()
    {


        jug.SPEED = 2000;
        //SPEED = 1500;

        yield return new WaitForSeconds(1.5f);

        jug.SPEED = 1200;

        //SPEED = 1200;


    }

}
