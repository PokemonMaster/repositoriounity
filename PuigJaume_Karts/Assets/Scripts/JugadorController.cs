﻿using UnityEngine;

public class JugadorController : MonoBehaviour
{

    #region Constants
    public float SPEED = 800f;
    public float ROTATE_SPEED;
    public float ROTATE_SPEED_ARRIBA;
    #endregion Constants

    #region Fields
    private Vector3 leftBound;
    private Vector3 rightBound;
    #endregion Fields

    #region Methods
    public void FixedUpdate()
    {
        ProcessInput();

        if (this.gameObject.name.Equals("Player"))
        {
            if (transform.position.y <= -30.15f)
            {

                RenderSettings.fogColor = Color.blue;
                RenderSettings.fogDensity = 0.02f;

            }
            else
            {

                RenderSettings.fogDensity = 0f;

            }
        }

    }

    private void ProcessInput()
    {

        if (Input.GetKey("space"))
        {
            this.GetComponent<Rigidbody>().AddForce(this.transform.forward * SPEED * Time.deltaTime);
        }
        if (Input.GetKey("left") || Input.GetKey("a"))
        {
            /*
             * RotateAround, gira en funció d'un punt (fent servir un punt de referencia, un vector de rotació, i un angle)
             * El punt de referencia seria el centre de l'objecte. si vols rotar sobre tu mateix seria la propia position (recordem que la position implica les coordenades centrals)
             * el vector pots fer servir un dels tres vectors base
             *  transform.up -> gira al voltant de l'eix Y (fletxa verda)
             *  transform.forward -> gira al voltant de l'eix Z (fletxa blava)
             *  transform.right -> gira al voltant de l'eix X (fletxa vermella)
             * 
             * 
             * */


            /*
             * Time.deltaTime torna el temps entre Frames. D'aquesta forma t'assegures que la velocitat sigui independent del framerate. (un framerate mes baix fara que es mogui mes per frame)
             **/

            transform.RotateAround(transform.position, transform.up, Time.deltaTime * -ROTATE_SPEED);


        }
        if (Input.GetKey("right") || Input.GetKey("d"))
        {

            transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATE_SPEED);


        }

        if (Input.GetKey("up") || Input.GetKey("w"))
        {

            this.GetComponent<Rigidbody>().AddForce(this.transform.up * ROTATE_SPEED_ARRIBA * Time.deltaTime);
            //transform.RotateAround(transform.position, transform.right, Time.deltaTime * -ROTATE_SPEED_ARRIBA);

        }
        if (Input.GetKey("down") || Input.GetKey("s"))
        {

            this.GetComponent<Rigidbody>().AddForce(this.transform.up * -ROTATE_SPEED_ARRIBA * Time.deltaTime);
            //transform.RotateAround(transform.position, transform.right, Time.deltaTime * ROTATE_SPEED_ARRIBA);

        }
    }

    

    #endregion Methods
}