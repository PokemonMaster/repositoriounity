﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BestTiempo", menuName = "Tiempito", order = 51)]
public class BestTime : ScriptableObject
{

    [SerializeField]
    public float bestTime;

}
