﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    //basado en el controller de nuestro muy honorable profesor Marc Albareda.

    public PlayerController player;

    public Camera gameCamera;
    public Move background;

    public Text score;
    public static int puntos = 0;

    //todos los prefabs disponibles
    public GameObject[] blockPrefabs;
    //puntero, apunta el final de los bloques generados
    private float puntero;
    //spawn, la variable extra que tiene que "mirar" el puntero para saber si generar un nuevo bloque
    private float spawn = 45;


    // Start is called before the first frame update
    void Start()
    {
        Instantiate(blockPrefabs[0]);

        score.text = "El bardo se los garcha a todos";
        score.color = Color.black;
        score.fontSize = 15;
        puntos = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // Actualizamos el score con la puntuación actual.
        score.text = "Score:"+puntos;
        score.color = Color.black;
        score.fontSize = 35;

        gameCamera.transform.position = new Vector3(
            player.transform.position.x+5,
            gameCamera.transform.position.y,
            gameCamera.transform.position.z);
        background.transform.position = new Vector3(
            player.transform.position.x + 5,
            background.transform.position.y,
            background.transform.position.z);
            

        if (player != null && this.puntero < player.transform.position.x + spawn)
        {
            // Sumamos puntos para el score.
            puntos += 10;
            //instanciar hace aparecer en la escena activa
            GameObject newBlock = Instantiate(blockPrefabs[Random.Range(1, blockPrefabs.Length)]);
            //cogemos el tamaño, que seria la escala (tamaño) de x, del primer hijo (el suelo) del bloque prefab
            float size = newBlock.transform.GetChild(0).transform.localScale.x;
            //ponemos la posicion del tamaño instanciado en la posicion del puntero + la mitad de su tamaño (la posicion que tenemos que indicarle es su centro absoluto)
            newBlock.transform.position = new Vector2(puntero + size / 2, 0);
            //aumentamos el puntero en el tamaño del bloque
            puntero += size;
        }

    }
}
